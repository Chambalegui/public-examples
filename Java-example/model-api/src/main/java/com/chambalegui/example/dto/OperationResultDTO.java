package com.chambalegui.example.dto;

import java.io.Serializable;

public class OperationResultDTO<T> implements Serializable {

    /**
     *
     */
    private boolean status;

    /**
     *
     */
    private String message;

    /**
     *
     */
    private T result;

    /**
     *
     */
    private Throwable exception;

    /**
     *
     * @return
     */
    public boolean isStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     *
     * @return
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     *
     * @return
     */
    public T getResult() {
        return result;
    }

    /**
     *
     * @param result
     */
    public void setResult(T result) {
        this.result = result;
    }

    /**
     *
     * @return
     */
    public Throwable getException() {
        return exception;
    }

    /**
     *
     * @param exception
     */
    public void setException(Throwable exception) {
        this.exception = exception;
    }
}
