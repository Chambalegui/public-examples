package com.chambalegui.example.dao.impl;

import com.chambalegui.example.dao.ProductDao;
import com.chambalegui.example.model.Product;

/**
 *
 */
public class ProductDaoImpl extends GenericDaoImpl<Product> implements ProductDao {

    /**
     *
     */
    public ProductDaoImpl() {
        super(Product.class);
    }
}
