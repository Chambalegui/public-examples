package com.chambalegui.example.dao;

import com.chambalegui.example.model.Product;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 *
 * @param <T>
 */
public interface GenericDao<T> {

    /**
     *
     * @param object
     */
    public void persist(T object);

    /**
     *
     * @param id
     * @return
     */
    public T getById(Long id);

    /**
     *
     * @return
     */
    public List<T> getAll();

    /**
     *
     * @param id
     */
    public void delete(Long id);

}
