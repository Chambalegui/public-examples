package com.chambalegui.example.dao.impl;

import com.chambalegui.example.dao.GenericDao;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import java.util.List;

public class GenericDaoImpl<T> extends HibernateDaoSupport implements GenericDao<T> {

    /**
     *
     */
    private Class<T> entityClass;

    /**
     *
     * @param entityClass
     */
    public GenericDaoImpl(Class<T> entityClass) {
        this.entityClass = entityClass;
    }


    public void persist(T object) {
        this.getHibernateTemplate().saveOrUpdate(object);
        this.getHibernateTemplate().flush();
    }

    public T getById(Long id) {
        return (T) getHibernateTemplate().get(entityClass.getSimpleName(), id);
    }

    public List<T> getAll() {
        return (List<T>) getHibernateTemplate().findByExample(entityClass.getSimpleName());
    }

    public void delete(Long id) {
        T object = getById(id);
        this.getHibernateTemplate().delete(object);
        this.getHibernateTemplate().flush();
    }
}
