package com.chambalegui.example.dao;

import com.chambalegui.example.model.Product;

public interface ProductDao extends GenericDao<Product> {
}
