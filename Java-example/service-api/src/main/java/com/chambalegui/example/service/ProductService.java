package com.chambalegui.example.service;

import com.chambalegui.example.dto.OperationResultDTO;
import com.chambalegui.example.dto.ProductDTO;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public interface ProductService {

    /**
     *
     * @param productDto
     */
    OperationResultDTO<ProductDTO> addOrUpdate(ProductDTO productDto);

    /**
     *
     * @param id
     */
    OperationResultDTO<Boolean> delete(Long id);

    /**
     *
     * @param id
     * @return
     */
    OperationResultDTO<ProductDTO> getById(Long id);

    /**
     *
     * @return
     */
    OperationResultDTO<List<ProductDTO>> getAll();
}
