package com.chambalegui.example.service.common;

import com.chambalegui.example.dto.OperationResultDTO;

/**
 *
 */
public class BaseService {

    /**
     *
     * @param result
     * @param <T>
     * @return
     */
    protected <T> OperationResultDTO<T> generateSuccessOperationResult(T result){
        OperationResultDTO<T> operationResultDTO = new OperationResultDTO<T>();
        operationResultDTO.setStatus(true);
        operationResultDTO.setResult(result);
        return operationResultDTO;
    }

    /**
     *
     * @param result
     * @param e
     * @param <T>
     * @return
     */
    protected <T> OperationResultDTO<T> generateFailOperationResult(T result, Exception e){
        OperationResultDTO<T> operationResultDTO = new OperationResultDTO<T>();
        operationResultDTO.setStatus(false);
        operationResultDTO.setResult(result);
        operationResultDTO.setMessage(e.getMessage());
        return operationResultDTO;
    }
}
