package com.chambalegui.example.service.impl;

import com.chambalegui.example.dao.ProductDao;
import com.chambalegui.example.dto.OperationResultDTO;
import com.chambalegui.example.dto.ProductDTO;
import com.chambalegui.example.model.Product;
import com.chambalegui.example.service.ProductService;
import com.chambalegui.example.service.common.BaseService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class ProductServiceImpl extends BaseService implements ProductService {

    /**
     *
     */
    @Autowired
    private ProductDao productDao;

    public OperationResultDTO<ProductDTO> addOrUpdate(ProductDTO productDto) {
        try {
            // TODO Add converter
            productDao.persist(new Product());
            return generateSuccessOperationResult(productDto);
        } catch (Exception e) {
            return generateFailOperationResult(productDto, e);
        }
    }

    public OperationResultDTO<Boolean> delete(Long id) {
        try {
            productDao.delete(id);
            return generateSuccessOperationResult(true);
        } catch (Exception e) {
            return generateFailOperationResult(false, e);
        }
    }

    public OperationResultDTO<ProductDTO> getById(Long id) {
        try {
            Product product = productDao.getById(id);
            // TODO Add converter
            return generateSuccessOperationResult(new ProductDTO());
        } catch (Exception e) {
            return generateFailOperationResult(new ProductDTO(), e);
        }
    }

    public OperationResultDTO<List<ProductDTO>> getAll() {
        List<ProductDTO> productList = new ArrayList<ProductDTO>();
        try {
            List<Product> product = productDao.getAll();
            // TODO Add converter
            return generateSuccessOperationResult(productList);
        } catch (Exception e) {
            return generateFailOperationResult(productList, e);
        }
    }
}
