/**
 * Example components
 */
package com.chambalegui.example.controller.base;

import com.chambalegui.example.dto.OperationResultDTO;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Interface related to Base Controller
 */
public interface BaseController<T> {

    /**
     * Method that will be used for get all elements.
     */
    public OperationResultDTO<T> getAll();

    /**
     * Method that will be used for get a element.
     *
     * @param id Identifier related to element to search.
     */
    public OperationResultDTO<T> getById(@RequestParam("id") Long id);

    /**
     * Method that will be used for delete a element.
     *
     * @param id Identifier related to element to delete.
     */
    public OperationResultDTO<T> deleteById(@RequestParam("id") Long id);

    /**
     * Method that will be used for save or update a element.
     *
     * @param object Object with information.
     */
    public OperationResultDTO<T> addOrUpdate(@RequestParam("object") String object);
}
