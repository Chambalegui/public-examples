/**
 * Example components.
 */
package com.chambalegui.example.controller;

import com.chambalegui.example.controller.base.BaseController;
import com.chambalegui.example.dto.OperationResultDTO;
import com.chambalegui.example.dto.ProductDTO;
import com.chambalegui.example.service.ProductService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 *
 */
@RestController("/product")
public class ProductController implements BaseController {

    /**
     *
     */
    @Autowired
    private ProductService productService;

    @Override
    @RequestMapping(name = "/getAll",method = RequestMethod.GET)
    public OperationResultDTO<List<ProductDTO>> getAll() {
        return productService.getAll();
    }

    @Override
    @RequestMapping(name = "/getById/{id}",method = RequestMethod.GET)
    public OperationResultDTO<ProductDTO> getById(@RequestParam("id") Long id) {
        return productService.getById(id);
    }

    @Override
    @RequestMapping(name = "/deleteById/{id}",method = RequestMethod.POST)
    public OperationResultDTO<Boolean> deleteById(@RequestParam("id") Long id) {
        return productService.delete(id);
    }

    @Override
    @RequestMapping(name = "/addOrUpdate",method = RequestMethod.POST)
    public OperationResultDTO<ProductDTO> addOrUpdate(@RequestParam("object") String object) {
        OperationResultDTO<ProductDTO> operationResultDTO = new OperationResultDTO<>();
        ObjectMapper mapper = new ObjectMapper();

        try {
            ProductDTO productDTO = mapper.readValue(object, ProductDTO.class);
            operationResultDTO = productService.addOrUpdate(productDTO);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            operationResultDTO.setResult(null);
            operationResultDTO.setMessage(e.getMessage());
            operationResultDTO.setStatus(false);
        }

        return operationResultDTO;
    }
}
